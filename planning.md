FEATURE 1

* [x] Fork and clone the starter project from django-one-shot 
* [x] Create a new virtual environment in the repository directory for the project
* [x] Activate the virtual environment
* [x] Upgrade pip
* [x] Install django
* [x] Install black
* [x] Install flake8
* [x] Install djlint
* [x] Deactivate your virtual environment
* [x] Activate your virtual environment
* [x] Use pip freeze to generate a requirements.txt file
* [x] python manage.py test tests.test_feature_01

FEATURE 2

* [x] Create a Django project named brain_two so that the manage.py file is in the top directory
* [x] Create a Django app named todos and install it in the brain_two Django project in the INSTALLED_APPS list
  * [x] python manage.py startapp «name»
  * [x] add to INSTALLED_APPS
* [x] Run the migrations
* [x] Create a super user
* [x] python manage.py test tests.test_feature_02

FEATURE 3
* [x] create a TodoList model in the todos Django app
  * [x] name is string with max length of 100 char
  * [x] created_on is datetime, automatically set to when this was created
* [x] implicitly convert a string with the __str__ method that has the value of the name property
* [x] python manage.py test tests.test_feature_03

FEATURE 4

* [x] Register the TodoList model with the admin so that you can see it in the Django admin site.
* [x] python manage.py test tests.test_feature_04

FEATURE 5

* [x] create TodoItem in Django app
* [x] task is string, max of 100 char
* [x] due_date is datetime, should be optional
* [x] is_completed is boolean, should default to false
* [x] list is foreign key, should be related to the TodoList model and have a related name of "items" and automatically delete with cascae
* [x] __str__ method on the task property
* [x] python manage.py test tests.test_feature_05

FEATURE 6

* [x] Register the TodoItem model with the admin so that you can see it in the Django admin site.
* [x] open up the Django admin in your browser and try adding some TodoItems using the admin
* [x] python manage.py test tests.test_feature_06

FEATURE 7

* [x] Create a view that will get all of the instances of the TodoList model
  * [x] put them in the context for the template.
* [x] Register that view in the todos app for the path "" and the name "todo_list_list" in a new file named todos/urls.py.
* [x] Include the URL patterns from the todos app in the brain_two project with the prefix "todos/".
* [x] Create a template for the list view that complies with the following specifications.
* [x] python manage.py test tests.test_feature_07

FEATURE 8

* [x] Create a view that shows the details of a particular to-do list, including its tasks
* [x] In the todos urls.py file, register the view with the path "<int:pk>/" and the name "todo_list_detail"
* [x] Create a template to show the details of the todolist and a table of its to-do items
* [x] Update the list template to show the number of to-do items for a to-do list
* [x] Update the list template to have a link from the to-do list name to the detail view for that to-do list
  * [x] the fundamental five in it
  * [x] a main tag that contains:
  * [x] div tag that contains:
  * [x] an h1 tag with the to-do list's name as its content
  * [x] an h2 tag that has the content "Tasks"
  * [x] a table that contains two columns with the headers "Task" and "Due date" with rows for each task in the to-do list
* [x] The to-do list view should now contain an a tag to the to-do list detail page for the indicated to-do list in the table's first column.
* [x] The second column should now show the total number of to-do items for a project.
* [x] python manage.py test tests.test_feature_08

FEATURE 9

* [x] Create a create view for the TodoList model that will show the name field in the form and handle the form submission to create a new TodoList
* [x] If the to-do list is successfully created, it should redirect to the detail page for that to-do list
* [x] Register that view for the path "create/" in the todos urls.py and the name "todo_list_create"
* [x] Create an HTML template that shows the form to create a new TodoList (see the template specifications below)
  * [x] There are two templates that need to be changed or created.
  * [x] a main tag that contains:
  * [x] div tag that contains:
  * [x] an h1 tag with the content "Create a new list"
  * [x] a form tag with method "post" that contains any kind of HTML structures but must include:
  * [x] an input tag with type "text" and name "name"
  * [x] a button with the content "Create"
  * [x] You need to add a link to the to-do list view after the h1 tag and before the table. The HTML that you add should be inside the div tag, after the h1 tag, and be:
  * [x] a p tag that contains:
  * [x] an a tag with an href attribute that points to the create to-do list URL and has content "Create a new list"
* [x] Add a link to the list view for the TodoList that navigates to the new create view
* [x] python manage.py test tests.test_feature_09

FEATURE 10

* [x] Create an update view for the TodoList model that will show the name. field in the form and handle the form submission to change an existing TodoList
* [x] If the to-do list is successfully edited, it should redirect to the detail page for that to-do list.
* [x] Register that view for the path /todos/<int:pk>/edit in the todos urls.py and the name "todo_list_update".
* [x] Create an HTML template that shows the form to edit a new TodoList (see the template specifications below).
  * [x] the fundamental five in it
  * [x] a main tag that contains:
  * [x] div tag that contains:
  * [x] an h1 tag with the content "Update list"
  * [x] a form tag with method "post" that contains any kind of HTML structures but must include:
  * [x] an input tag with type "text" and name "name"
  * [x] a button with the content "Update"
  * [x] You need to add a link to the to-do list detail view after the h1 tag and before the table. The HTML that you add should be inside the div tag, after the h1 tag, and be:
  * [x] a p tag that contains:
  * [x] an a tag with an href attribute that points to the create todo list URL and has content "Edit"
* [x] Add a link to the list view for the TodoList that navigates to the new update view.
* [x] python manage.py test tests.test_feature_10

FEATURE 11

* [x] Create a delete view for the TodoList model that will show a delete button and handle the form submission to delete an existing TodoList.
* [x] If the to-do list is successfully deleted, it should redirect to the to-do list list view.
* [x] Register that view for the path /todos/<int:pk>/delete in the todos urls.py and the name "todo_list_delete".
* [x] Create an HTML template that shows the form to delete a new TodoList (see the template specifications below).
* [x] The resulting HTML from a request to the path http://localhost:8000/todos/<int:pk>/delete/ should result in HTML that has:
  * [x] the fundamental five in it
  * [x] a main tag that contains:
  * [x] div tag that contains:
  * [x] a h1 tag that contains the text, "Are you sure?"
  * [x] a form tag with method "post" that contains any kind of HTML structures but must include:
  * [x] a button with the content "Delete"
  * [x] Update the todo detail view
  * [x] You need to add a link to the to-do list detail view after the h1 tag and before the table. The HTML that you add should be inside the div tag, after the h1 tag, and be:
  * [x] a p tag that contains:
  * [x] an a tag with an href attribute that points to the create todo list URL and has content "Delete"
* [x] Add a link to the detail view for the TodoList that navigates to the new delete view.
* [x] python manage.py test tests.test_feature_11

FEATURE 12

* [x] Create a create view for the TodoItem model that will show the task field in the form and handle the form submission to create a new TodoItem.
* [x] If the to-do list is successfully created, it should redirect to the detail page for that to-do list.
* [x] Register that view for the path "items/create/" in the todos urls.py and the name "todo_item_create".
* [x] Create an HTML template that shows the form to create a new TodoItem (see the template specifications below).
* [x] The resulting HTML from a request to the path http://localhost:8000/todos/items/create/  should result in HTML that has:
  * [x] the fundamental five in it
  * [x] a main tag that contains:
  * [x] div tag that contains:
  * [x] an h1 tag with the content "Create a new item"
  * [x] a form tag with method "post" that contains any kind of HTML structures but must include:
  * [x] an input tag with type "text" and name "task"
  * [x] an input tag with type "text" and name "due date"
  * [x] an input tag with type "checkbox" and name "is_completed"
  * [x] a select list with the name "list"
  * [x] a button with the content "Create"
  * [x] Update the todo list view
  * [x] You need to add a link to the to-do list view after the h1 tag and before the table. The HTML that you add should be inside the div tag, after the h1 tag, and be:
  * [x] a p tag that contains
  * [x] an a tag with an href attribute that points to the create to-do item URL and has content "Create a new item"
* [x] Add a link to the list view for the TodoList that navigates to the new create view.
* [x] python manage.py test tests.test_feature_12

FEATURE 13

* [x] Create an update view for the TodoItem model that will show the task field, the due date field, an is_completed checkbox, and a select list showing the to-do lists in the form and handle the form submission to change an existing TodoItem.
* [x] If the to-do item is successfully edited, it should redirect to the detail page for the to-do list.
* [x] Register that view for the path /todos/items/<int:pk>/edit in the todos urls.py and the name "todo_item_update".
* [x] Create an HTML template that shows the form to edit a TodoItem (see the template specifications below).
* [x] The resulting HTML from a request to the path http://localhost:8000/todos/<int:pk>/edit/ should result in HTML that has:
  * [x] the fundamental five in it
  * [x] a main tag that contains:
  * [x] div tag that contains:
  * [x] an h1 tag with the text "Update item"
  * [x] a form tag with method "post" that contains any kind of HTML structures but must include:
  * [x] an input tag with type "text" and name "task"
  * [x] an input tag with type "text" and name "due date"
  * [x] an input tag with type "checkbox" and name "is_completed"
  * [x] a select list with the name "list"
  * [x] a button with the content "Update"
  * [x] You need to add links to the to-do list detail view. Each task name in the table should link to the correct to-do list item's edit page url.
* [x] Add a link to the list view for the TodoList that navigates to the new update view.
* [x] python manage.py test tests.test_feature_13